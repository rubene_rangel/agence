export const helpersMixins = {
  methods: {
    portuguesMonths(month) {
      switch (Number(month)) {
        case 1:
          return 'Janeiro';
        case 2:
          return 'Fevereiro';
        case 3:
          return 'Março';
        case 4:
          return 'Abril';
        case 5:
          return 'Maio';
        case 6:
          return 'Junho';
        case 7:
          return 'Julho';
        case 8:
          return 'Agosto';
        case 9:
          return 'Septembro';
        case 10:
          return 'Octubro';
        case 11:
          return 'Novembro';
        case 12:
          return 'Dezembro';
      }
    },
    withCommas(num) {
      return (
        'R$ ' +
        num
          .toFixed(2)
          .replace('.', ',')
          .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
      );
    },
    color() {
      let color = Math.floor(Math.random() * 10);
      switch (Number(color)) {
        case 1:
          return 'red';
          break;
        case 2:
          return 'blue';
          break;
        case 3:
          return 'purple';
          break;
        case 4:
          return 'yellow';
          break;
        case 5:
          return '#ffce56';
          break;
        case 6:
          return '#cc65fe';
          break;
        case 7:
          return '#36a2eb';
          break;
        case 8:
          return '#ff6384';
          break;
        case 9:
          return '#ffcc2a';
          break;
        case 0:
          return '#194976';
          break;
      }
    }
  }
};
