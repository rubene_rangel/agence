const state = {
  drawer: true
};

const mutations = {
  ['TOOGLE_DRAWER'](state, payload) {
    state.drawer = payload;
  }
};

const actions = {
  //
};

export default {
  namespaced: true,
  state,
  mutations
};
