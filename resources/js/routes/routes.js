import Dashboard from '../views/Dashboard';
import Projetos from '../views/Projetos';
import Administrativo from '../views/Administrativo';
import Comercial from '../views/Comercial';
import Financeiro from '../views/Financeiro';
import Usuario from '../views/Usuario';

let routes = [
  {
    path: '/',
    component: Dashboard,
    name: 'dashboard'
  },
  {
    path: '/projetos',
    component: Projetos,
    name: 'projetos'
  },
  {
    path: '/administrativo',
    component: Administrativo,
    name: 'administrativo'
  },
  {
    path: '/comercial',
    component: Comercial,
    name: 'comercial'
  },
  {
    path: '/financeiro',
    component: Financeiro,
    name: 'financeiro'
  },
  {
    path: '/usuario',
    component: Usuario,
    name: 'usuario'
  },
  {
    path: '/logout',
    name: 'logout'
  }
];

export default routes;
