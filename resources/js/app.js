//import './bootstrap';
/**
 * Function Global
 */
window.clone = obj => JSON.parse(JSON.stringify(obj));

window.toastr = require('toastr');

toastr.options = {
  closeButton: false,
  debug: false,
  newestOnTop: false,
  progressBar: false,
  positionClass: 'toast-top-right',
  preventDuplicates: false,
  onclick: null,
  showDuration: '300',
  hideDuration: '1000',
  timeOut: '5000',
  extendedTimeOut: '1000',
  showEasing: 'swing',
  hideEasing: 'linear',
  showMethod: 'fadeIn',
  hideMethod: 'fadeOut'
};

window.Vuetify = require('vuetify');

import './vuetify';

import Vuetify from 'vuetify';

import Vue from 'vue';
import router from './routes';

/**
 * Vue store
 */
import store from './store';

import Layout from './layouts/index';

Vue.use(Vuetify);

const app = new Vue({
  el: '#app',
  router,
  store,
  render: h => h(Layout),
  vuetify: new Vuetify({
    icons: {
      iconfont: 'mdi' // default - only for display purposes
    }
  })
});
