<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permissao extends Model
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'permissao_sistema';

  protected $primaryKey = 'co_usuario';

  public $incrementing = false;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'co_sistema', 
    'in_ativo',
    'co_tipo_usuario',
  ];

  public function user()
  {
    return $this->hasMany('App\User', 'co_usuario', 'co_usuario');
  }
}
