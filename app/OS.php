<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OS extends Model
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'cao_os';

  protected $primaryKey = 'co_os';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'co_os', 
    'co_usuario',
  ];
}
