<?php

namespace App\Http\Controllers;

use App\OS;
use Illuminate\Http\Request;
use App\Permissao;
use Illuminate\Support\Facades\DB;

class PerformanceController extends Controller
{
  public function collaborators(Request $request)
  {
    if ($request->ajax()) {
      $typeUser0 = $request->co_tipo_usuario[0];
      $typeUser1 = $request->co_tipo_usuario[1];
      $typeUser2 = $request->co_tipo_usuario[2];

      $collaborators = Permissao::with(array('user' => function ($query) {
        $query->select('co_usuario', 'no_usuario');
      }))
        ->where('co_sistema', '=', $request->co_sistema)
        ->where('in_ativo', '=', 'S')
        ->where(
          function ($query) use ($typeUser0, $typeUser1, $typeUser2) {
            $query->where('co_tipo_usuario', '=', $typeUser0)
              ->orWhere('co_tipo_usuario', '=', $typeUser1)
              ->orWhere('co_tipo_usuario', '=', $typeUser2);
          }
        )
        ->join('cao_usuario', 'cao_usuario.co_usuario', '=', 'permissao_sistema.co_usuario')
        ->orderBy('cao_usuario.no_usuario')
        ->get();

      return response()->json(['collaborators' => $collaborators]);
    }
  }

  public function receita(Request $request)
  {
    if ($request->ajax()) {
      $firstDateFrom = $request->startMonth . '-01';
      $lastDateTo = date('Y-m-t', strtotime($request->endMonth . '-01'));

      $performanceCollaborator = OS::select(
        'cao_os.co_os AS OS',
        'cao_usuario.no_usuario',
        'cao_salario.brut_salario',
        DB::raw('MONTH(cao_fatura.data_emissao) AS month_emissao'),
        DB::raw('YEAR(cao_fatura.data_emissao) AS year_emissao'),
        'cao_os.co_usuario',
        'cao_fatura.co_cliente',
        'cao_fatura.co_sistema',
        'cao_fatura.comissao_cn',
        'cao_fatura.valor',
        'cao_fatura.total_imp_inc'
      )
        ->join('cao_fatura', 'cao_os.co_os', '=', 'cao_fatura.co_os')
        ->join('cao_usuario', 'cao_usuario.co_usuario', '=', 'cao_os.co_usuario')
        ->join('cao_salario', 'cao_usuario.co_usuario', '=', 'cao_salario.co_usuario')
        ->whereBetween('cao_fatura.data_emissao', [$firstDateFrom, $lastDateTo])
        ->whereIn('cao_os.co_usuario', $request->collaborators)
        ->orderBy('cao_usuario.no_usuario')
        ->get();


      return response()->json([
        'performanceCollaborator' => $performanceCollaborator->groupBy(['no_usuario', 'month_emissao'])
      ]);
    }
  }

  public function barChart(Request $request)
  {
    if ($request->ajax()) {
      // $request->collaborators = !$request->collaborators ? ['anapaula.chiodaro'] : $request->collaborators;
      $firstDateFrom = $request->startMonth . '-01';
      $lastDateTo = date('Y-m-t', strtotime($request->endMonth . '-01'));

      $performanceCollaborator = OS::select(
        'cao_os.co_os AS OS',
        'cao_usuario.no_usuario',
        'cao_salario.brut_salario',
        DB::raw('MONTH(cao_fatura.data_emissao) AS month_emissao'),
        DB::raw('YEAR(cao_fatura.data_emissao) AS year_emissao'),
        'cao_os.co_usuario',
        'cao_fatura.co_cliente',
        'cao_fatura.co_sistema',
        'cao_fatura.comissao_cn',
        'cao_fatura.valor',
        'cao_fatura.total_imp_inc'
      )
        ->join('cao_fatura', 'cao_os.co_os', '=', 'cao_fatura.co_os')
        ->join('cao_usuario', 'cao_usuario.co_usuario', '=', 'cao_os.co_usuario')
        ->join('cao_salario', 'cao_usuario.co_usuario', '=', 'cao_salario.co_usuario')
        ->whereBetween('cao_fatura.data_emissao', [$firstDateFrom, $lastDateTo])
        ->whereIn('cao_os.co_usuario', $request->collaborators)
        ->orderBy('cao_usuario.no_usuario')
        ->get();


      return response()->json([
        'chartCollaborators' => $performanceCollaborator->groupBy(['no_usuario', 'month_emissao'])
      ]);
    }
  }
}
