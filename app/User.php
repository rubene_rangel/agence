<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;


// class User extends Authenticatable
class User extends Model
{
    //use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cao_usuario';

    protected $primaryKey = 'co_usuario';

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'co_usuario', 
        'no_usuario',
    ];

    public function permissao() 
    {
      return $this->belongsTo('App\Permissao', 'co_usuario', 'co_usuario');
    }
}
