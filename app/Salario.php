<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salario extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'cao_salario';

  protected $primaryKey = 'co_usuario';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'brut_salario',
  ];

  protected $casts = [
    'brut_salario' =>  'float',
  ];
}
