<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fatura extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'cao_fatura';

  protected $primaryKey = 'co_fatura';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'co_fatura',
    'co_cliente',
    'co_sistema',
    'data_emissao',
    'valor',
    'total_imp_inc',
    'comissao_cn'
  ];

  protected $casts = [
    'co_fatura' =>  'integer',
    'co_cliente' =>  'integer',
    'co_sistema' =>  'integer',
    'data_emissao' =>  'date',
    'valor' =>  'float',
    'total_imp_inc' =>  'float',
    'comissao_cn' => 'float'
  ];
}
